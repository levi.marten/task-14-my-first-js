// -----------------------------------------------------------------------------------------------------------------
// JS developer course February 2020 - Moodle Task 14
// -----------------------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------------
// JavaScript Task 1 - part 1. Four functions just doing basic math things.
// -----------------------------------------------------------------------------------------------------------------

// Function multiplies two arguments. The answer is then logged in the console.

function multiply(argument1, argument2) {
    const answer = argument1 * argument2;
    console.log(answer);
}

multiply(5, 5);


// Function adds three arguments and returns the sum.

function add(no1, no2, no3) {
    const sum = no1 + no2 + no3;
    console.log(sum);
}

add(3, 4, 6);


// Function divides numerator with 2 and logs the fraction in console.

function divide(numerator) {
    const denominator = 2;
    const fraction = numerator / denominator;
    console.log(fraction);
}

divide(10);


// Function adds two arguments and multiplies that result with a third argument.

function calculate(x, y, z) {
    const result = (x + y) * z;
    console.log(result);
}

calculate(10, 3, 5);

// ----------------------------------------------------------------------------------------------------------------------
// JavaScript Task 1 - part 2. 3 basic parts of JS described in code with comments (Block scoped, Hoisting and Coercion).
// ----------------------------------------------------------------------------------------------------------------------

// Block scoped variables explained. -------------------------------------------------------------------------------

if (true) {                                         // If statement that will always run, just to get a block of code in the program.
    var myVar = "var-variable";                   // Assigning value to var variable.
    console.log(myVar + " inside code block.");      // Adding text to variable in code block and displaying it in the console.
    let myLet = "let-variable";                   // Assigning value to let variable.
    console.log(myLet + " inside code block.");      // Adding text to variable in code block and displaying it in the console.
    let myConst = "const-variable";                 // Assigning value to const variable.
    console.log(myConst + " inside code block.");    // Adding text to variable in code block and displaying it in the console.
}

if (typeof myVar !== 'undefined') {                 // Displaying variable again, outside of code block if it still exists...
    console.log(myVar + " outside of code block means it is not block scoped.");
} else {                                        // Shows that variable is block scoped if it has been cleared from memory.
    console.log("var variables are block scoped and you've already used myVar in another code block.")
}

if (typeof myLet !== 'undefined') {                 // Displaying variable again, outside of code block if it still exists...
    console.log(myLet + " outside of code block means it is not block scoped.");
} else {                                        // Shows that variable is block scoped if it has been cleared from memory.
    console.log("let variables are block scoped and you've already used myLet in another code block.")
}

if (typeof myConst !== 'undefined') {               // Displaying variable again, outside of code block if it still exists...
    console.log(myConst + " outside of code block means it is not block scoped.");
} else {                                        // Shows that variable is block scoped if it has been cleared from memory.
    console.log("const variables are block scoped and you've already used myConst in another code block.")
}

// ----------------------------------------------------------------------------------------------------------------

// Hoisting explained. --------------------------------------------------------------------------------------------

// Function is called before it is declared! (GASP!)
doSomething();

// Function is declared after being called. If this works it means that function declarations are "moved up" in priority during the context creation phase.
function doSomething() {
    console.log("This string shows that functions can be hoisted when the context creation phase is run, before code is being executed.");
}

// ---------------------------------------------------------------------------------------------------------------

// Six examples of Coercion --------------------------------------------------------------------------------------

console.log('This string is text, followed by math that JS translates to text without me telling it to and showing the number 55. ' + 5 + 5 + '.');
// Automatically turns numbers into strings and concatenates instead of trying to do math.

console.log(5 + 5 + ' This text follows the number 10, even though I typed just like above, except with the numbers before the string.')
// Automatically does math and then follows by turning the result into a string.

console.log((true + 5) / 3, 4 * false)
// Shows 2 and 0, even though I mixed numbers and Booleans.

console.log('A string' + ' plus a string' + ' plus another string' + ' concatenates.')
// Concatenation of different text strings. 

console.log(3 * 'hello')
// Displays not a number because I am trying to multiply a string with text.


console.log(3 * '4')
// Displays 12 because a single number within a string is in this case interpreted as a number and math is performed.

// ----------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------------------
// Employee function that can be called upon with New -------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------

function Employee(employeeNumber, name, surname, email) {
    this.employeeNumber = employeeNumber;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.fullName = function() {
       console.log(this.name + ' ' + this.surname);
    };
    this.contactCard = function() {
       console.log(this.employeeNumber + ' ' + this.name + ' ' + this.surname + ' ' + this.email);
    };
};

const mrMarten = new Employee('119 28', 'Levi', 'Mårten', 'levi.marten@outlook.com');

// ----------------------------------------------------------------------------------------------------------------


// Project as an object -------------------------------------------------------------------------------------------

const project = {
    projectName: "The Superamazing Most Disturbingly Perfectest JavaScript Code Project Ever",
    projectDescription: "Some definitely important information about this project. As you can tell from the headline, this is no ordinary project.",
    projectManager: "Donald Duck",
    projectStartingDate: "February 26th 2020",
    projectEndingDate: "April 24th 2020",
    linesOfCodeInProject: "2000000000000000000000000000000",
}

// ----------------------------------------------------------------------------------------------------------------
